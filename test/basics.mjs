import test from 'p-tape'

import makePrefixedLogger from '../mpl.node'

test('basics', async(t) => {
  t.plan(1)
  const testLogger = {
    msgs: [],
    info(...args) { this.msgs.push([':', ...args]) },
    error(...args) { this.msgs.push(['!', ...args]) },
    kurzeFrage(...args) { this.msgs.push(['?', ...args]) },
  }
  const pl = makePrefixedLogger(testLogger, 'Prio51+')

  pl.error('Findet nicht statt', { foo: 123 })
  pl.kurzeFrage('Location?', 'Kolos-Saal')
  pl.info('Das ist korrekt', { bar: 456 })
  pl.error('Das soll nicht sein', { qux: 789 })

  t.deepEqual(testLogger.msgs, [
    ['!', 'Prio51+', 'Findet nicht statt', { foo: 123 }],
    ['?', 'Prio51+', 'Location?', 'Kolos-Saal'],
    [':', 'Prio51+', 'Das ist korrekt', { bar: 456 }],
    ['!', 'Prio51+', 'Das soll nicht sein', { qux: 789 }],
  ])
})
