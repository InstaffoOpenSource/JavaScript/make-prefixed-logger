﻿
<!--#echo json="package.json" key="name" underline="=" -->
@instaffogmbh/make-prefixed-logger
==================================
<!--/#echo -->

<!--#echo json="package.json" key="description" -->
Log arguments with your prefixes prefixed.
<!--/#echo -->

* 📣 💼 💰 Looking for a tech job?
  Try our [reverse recruiting service](https://instaffo.com/).



API
---

This module exports one function:

### const pl = makePrefixedLogger(origLogger[, ...prefixArgs])

Returns a function that concatenates all its arguments to the `prefixArgs`,
then proxies the combined arguments to `origLogger`.
Similarly, `pl` will carry proxy methods for all methods found on `origLogger`.



<!--#toc stop="scan" -->


&nbsp;


License
-------
<!--#echo json="package.json" key=".license" -->
MIT
<!--/#echo -->
