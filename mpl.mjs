function makePrefixedLogger(origLogger, ...pre) {
  if (!origLogger) { throw new TypeError('No original logger given!') }
  function logger(...args) { origLogger(...pre, ...args) }
  Object.keys(origLogger).forEach((mtdName) => {
    const mtdFunc = origLogger[mtdName]
    if ((typeof mtdFunc) !== 'function') { return }
    logger[mtdName] = (...args) => mtdFunc.apply(origLogger, pre.concat(args))
  })
  return logger
}

export default makePrefixedLogger
